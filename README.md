### ANSIBLE ROLE FOR POSTFIX USING MULTIPOSTFIX
Set up a postfix server in Centos like system using multipostfix to send mails in bunch

#### Requirements
Ansible
CentOS like platform with SSH access
Available yum repositories, using publics or spacewalk
User which can become root 

#### Variables
 * Ips that we will use
 * `postfix_install` [default: `[postfix, mailutils, libsasl2-2, sasl2-bin, libsasl2-modules`]]: Packages to install
 * `postfix_hostname` [default: `{{ ansible_fqdn }}`]: Host name, used for `myhostname` and in `mydestination`
 * `postfix_mailname` [default: `{{ ansible_fqdn }}`]: Mail name (in `/etc/mailname`), used for `myorigin`
 * `postfix_aliases` [default: `[]`]: Aliases to ensure present in `/etc/aliases`
 * `postfix_inet_interfaces` [default: `all`]: Network interfaces to bind (Postfix setting: [`inet_interfaces`](http://www.postfix.org/postconf.5.html#inet_interfaces))
 * `postfix_relayhost` [default: `false` (no relay host)]: Hostname to relay all email to
 * `postfix_relayhost_port` [default: 587]: Relay port (on `postfix_relayhost`, if set)
 * `postfix_relaytls` [default: `false`]: Use TLS when sending with a relay host
 * `postfix_sasl_user` [default: `postmaster@{{ ansible_domain }}`]: SASL relay username
 * `postfix_sasl_password` [default: `k8+haga4@#pR`]: SASL relay password **Make sure to change!**

#### Dependencies
None
#### Example

##### License
MIT
